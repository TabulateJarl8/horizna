import json
from os import environ as env
from urllib.parse import quote_plus, urlencode

from authlib.integrations.flask_client import OAuth
import dotenv

from flask import Flask, render_template, abort, redirect, session, url_for
from flask_sqlalchemy import SQLAlchemy
from jinja2 import TemplateNotFound

# load dotenv
ENV_FILE = dotenv.find_dotenv()
if ENV_FILE:
	dotenv.load_dotenv(ENV_FILE)

# initialize app
app = Flask(
	__name__,
	template_folder='templates/'
)

# Set variables and constants
app.secret_key = env.get("APP_SECRET_KEY")

app.config['SQLALCHEMY_DATABASE_URI'] = env.get("SQLALCHEMY_URL")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

oauth = OAuth(app)

oauth.register(
	"auth0",
	client_id=env.get("AUTH0_CLIENT_ID"),
	client_secret=env.get("AUTH0_CLIENT_SECRET"),
	client_kwargs={
		"scope": "openid profile email",
	},
	server_metadata_url=f'https://{env.get("AUTH0_DOMAIN")}/.well-known/openid-configuration'
)


# routes
@app.route('/', defaults={'page': 'index'})
@app.route('/<page>')
def home(page):
	try:
		return render_template(f'{page}.html')
	except TemplateNotFound:
		abort(404)

@app.route("/login/")
def login():
	return oauth.auth0.authorize_redirect(
		redirect_uri=url_for("callback", _external=True)
	)

@app.route("/callback/", methods=["GET", "POST"])
def callback():
	token = oauth.auth0.authorize_access_token()
	session["user"] = token
	return redirect("/")

@app.route("/logout/")
def logout():
	session.clear()
	return redirect(
		"https://" + env.get("AUTH0_DOMAIN")
		+ "/v2/logout?"
		+ urlencode(
			{
				"returnTo": url_for("home", _external=True),
				"client_id": env.get("AUTH0_CLIENT_ID"),
			},
			quote_via=quote_plus,
		)
	)